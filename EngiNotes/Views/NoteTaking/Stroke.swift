//
//  Stroke.swift
//  EngiNotes
//
//  Created by Michael on 5/2/19.
//  Copyright © 2019 Feel Good Technology. All rights reserved.
//

import UIKit

struct Stroke {
    let startPoint: CGPoint
    let endPoint: CGPoint
    let color: CGColor
}
