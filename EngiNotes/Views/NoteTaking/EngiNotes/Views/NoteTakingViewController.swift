//
//  NoteTakingViewController.swift
//  EngiNotes
//
//  Created by Michael on 4/15/19.
//  Copyright © 2019 Feel Good Technology. All rights reserved.
//

import UIKit
import PDFKit

class NoteTakingViewController: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var pdfView: UIView!
    
    var documentSet: DocumentModel! {
        didSet {
            self.title = documentSet?.title
        }
    }
    
    //MARK: - Lifecycle
    
    override func loadView() {
        super.loadView()
        
        navigationController?.navigationBar.prefersLargeTitles = false
        configPDFView()
    }
    
    //MARK: - Functions
    
    private func configPDFView() {
        let pdfViewer = PDFView(frame: .zero)
        
        pdfViewer.translatesAutoresizingMaskIntoConstraints = false
        
        pdfView.addSubview(pdfViewer)
        pdfViewer.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        pdfViewer.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        pdfViewer.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        pdfViewer.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        setDocument(to: documentSet, with: pdfViewer)
    }
    
    private func setDocument(to document: DocumentModel?, with view: PDFView) {
        guard let validDocument = document else {
            return
        }
        
        let pdf = PDFDocument(url: validDocument.url)
        //let drawingPDF = CGPDFDocument(validDocument.url as CFURL)
        
        view.document = pdf
    }
    
    //MARK: - IBActions
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    
}
