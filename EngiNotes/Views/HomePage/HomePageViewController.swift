//
//  ViewController.swift
//  EngiNotes
//
//  Created by Michael on 4/5/19.
//  Copyright © 2019 Feel Good Technology. All rights reserved.
//

import UIKit

class HomePageViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    @IBOutlet weak var CollectionView: UICollectionView!
    let reuseIdentifier = "HomePageCollectionViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CollectionView.dataSource = self
        navigationController?.navigationBar.prefersLargeTitles = true
        DocumentFunctions.readDocument(completion: { [weak self] in
          // completion handler is called
            self?.CollectionView.reloadData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Data.documentModel.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HomePageCollectionViewCell
        if indexPath.row == 0 {
            cell.showCreateCell()
        }
        else {
            let document = Data.documentModel[indexPath.item - 1]
            cell.displayContent(document: document)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedRow = indexPath.row
        if selectedRow == 0 {
            print("add button selected")
            return
        } else {
            let documentGet = Data.documentModel[selectedRow - 1]
            let storyboard = UIStoryboard.init(name: "NoteTaking", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: "NoteTakingViewController") as? NoteTakingViewController else {
                return
            }
            vc.documentSet = documentGet
            
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }
}

