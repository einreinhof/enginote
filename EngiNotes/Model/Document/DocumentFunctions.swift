//
//  DocumentFunctions.swift
//  EngiNotes
//
//  Created by Michael on 4/5/19.
//  Copyright © 2019 Feel Good Technology. All rights reserved.
//

import Foundation

class DocumentFunctions {
    static func createDocument(DocumentModel: DocumentModel)  {
        
    }
    static func readDocument(completion: @escaping () -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
            if Data.documentModel.count == 0 {
                Data.documentModel.append(DocumentModel(title: "Steam Table"))
                Data.documentModel.append(DocumentModel(title: "pve-admin-guide"))
                //Data.documentModel.append(DocumentModel(title: "Transport Homework"))
            }
        }
        DispatchQueue.main.async {
            completion()
        }
    }
    static func updateDocument(DocumentModel: DocumentModel) {
        
    }
    static func deleteDocument(DocumentModel: DocumentModel) {
        
    }
}
