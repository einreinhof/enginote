//
//  HomePageCollectionViewCell.swift
//  EngiNotes
//
//  Created by Michael on 4/5/19.
//  Copyright © 2019 Feel Good Technology. All rights reserved.
//

import UIKit

class HomePageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var previewImage: UIImageView!
    @IBOutlet weak var documentTitle: UILabel!
    
    var document: DocumentModel!
    
    func displayContent(document: DocumentModel) {
        self.document = document
        self.previewImage.image = drawPDFfromURL(url: document.url)
        self.documentTitle.text = document.title
    }
    
    func showCreateCell() {
        self.previewImage.image = UIImage(named: "DashAdd")
        self.documentTitle.text = "Create..."
    }
    
    func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }
        
        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)
            
            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
            
            ctx.cgContext.drawPDFPage(page)
        }
        
        return img
    }
}
