//
//  UIImageAndImage.swift
//  EngiNotes
//
//  Created by Michael on 4/8/19.
//  Copyright © 2019 Feel Good Technology. All rights reserved.
//

import UIKit

extension UIView {
    
    var imageShot: UIImage{
        
        get{
            //self.layer.shouldRasterize = true
            
            //print(self.contentScaleFactor)
            //UIGraphicsBeginImageContextWithOptions( self.bounds.size, false, self.contentScaleFactor)
            
            UIGraphicsBeginImageContextWithOptions( self.bounds.size, false, 3)
            self.layer.render(in: UIGraphicsGetCurrentContext()!)
            let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
            UIGraphicsEndImageContext();
            return image;
        }
    }
    
    
    
    func savePNG(_ filePath: String){
        if let data = self.imageShot.pngData(){
            try? data.write(to: URL(fileURLWithPath: filePath), options: [.atomic])
        }
    }
    
    
    func saveJPEG(_ filePath: String, quality:CGFloat){
        if let data = self.imageShot.jpegData(compressionQuality: 0.25){
            try? data.write(to: URL(fileURLWithPath: filePath), options: [])
        }
    }
    
}
//UIImageJPEGRepresentation(self.imageShot, quality)
