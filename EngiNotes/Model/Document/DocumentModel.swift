//
//  DocumentModel.swift
//  EngiNotes
//
//  Created by Michael on 4/5/19.
//  Copyright © 2019 Michael Einreinhof. All rights reserved.
//

import Foundation

class DocumentModel {
    var url: URL
    var title: String
    //var previewImage: 
    
    init(title: String) {
        self.title = title
        self.url = Bundle.main.url(forResource: title, withExtension: ".pdf")!
    }
}

extension DocumentModel: Equatable {
    static func == (lhs: DocumentModel, rhs: DocumentModel) -> Bool {
        return lhs.title == rhs.title && lhs.url == rhs.url
    }
}
